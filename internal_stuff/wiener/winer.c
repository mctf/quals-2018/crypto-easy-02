#include <stdio.h>
#include <gmp.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#define ARGS_COUNT 5
#define MAX_SIZE 10000
#define TEST_MSG 17ul

void sig_handler(int signo)
{
    printf("Received %d\n", signo);
    fprintf(stderr, "Can't compute this log because its too hard for this PC!\n");
    exit(EXIT_FAILURE);
}

void register_handler()
{
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGINT\n");
    if (signal(SIGBUS, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGBUS\n");
    if (signal(SIGFPE, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGFPE\n");
    if (signal(SIGILL, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGILL\n");
    if (signal(SIGSEGV, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGSEGV\n");
    if (signal(SIGSYS, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGSYS\n");
    if (signal(SIGXCPU, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGXCPU\n");
    if (signal(SIGXFSZ, sig_handler) == SIG_ERR)
        printf("\nCan't catch SIGXFSZ\n");
}

static inline void calc_d(mpz_t *cont_fraq, const size_t cont_fraq_n, mpz_t *e, mpz_t *n)
{
    mpz_t P_i, P_i_2, Q_i, Q_i_2, tmp1, tmp2;
    register size_t i;
    
    mpz_init_set(P_i, cont_fraq[0]);
    mpz_init_set_ui(Q_i, 1ul);
    mpz_init_set_ui(P_i_2, 1ul);
    mpz_init_set_ui(Q_i_2, 0ul);
    mpz_init(tmp1);
    mpz_init(tmp2);   
 
    for (i = 2; i < cont_fraq_n; ++i)
    {
        //P_i
        mpz_mul(tmp1, cont_fraq[i - 1], P_i);
        mpz_add(tmp2, tmp1, P_i_2);
        mpz_set(P_i_2, P_i);
        mpz_set(P_i, tmp2);
        
        //Q_i
        mpz_mul(tmp1, cont_fraq[i - 1], Q_i);
        mpz_add(tmp2, tmp1, Q_i_2);
        mpz_set(Q_i_2, Q_i);
        mpz_set(Q_i, tmp2);

        #ifdef DEBUG
        gmp_printf("P%d/Q%d = %Zd/%Zd\n", i, i, P_i, Q_i);
        #endif
        
        //(e*d-1)/k = phi(n), d = Q_i, k = P_i 
        mpz_mul(tmp1, *e, Q_i);
        mpz_sub_ui(tmp1, tmp1, 1ul);
        mpz_tdiv_qr(tmp1, tmp2, tmp1, P_i);
        #ifdef DEBUG
        gmp_printf("phi(n) = %Zd\n", tmp1);
        #endif

       
        if (mpz_cmp_ui(tmp2, 0ul) == 0)
        {
            mpz_set_ui(tmp2, TEST_MSG);
            mpz_powm(tmp2, tmp2, *e, *n);
            mpz_powm(tmp2, tmp2, Q_i, *n);
            
            if (mpz_cmp_ui(tmp2, TEST_MSG) != 0) continue;
            
            gmp_printf("d = 0x%Zx\n", Q_i);
            
            //(p+q)/2
            mpz_sub(tmp2, *n, tmp1);
            mpz_add_ui(tmp2, tmp2, 1ul);
            mpz_tdiv_q_ui(tmp2, tmp2, 2ul);
            #ifdef DEBUG
            gmp_printf("(p+q)/2 = %Zd\n", tmp2);
            #endif
            
            //(p-q)/2
            mpz_pow_ui(tmp1, tmp2, 2ul);
            mpz_sub(tmp1, tmp1, *n);
            mpz_sqrt(tmp1, tmp1);
            #ifdef DEBUG
            gmp_printf("(p-q)/2 = %Zd\n", tmp1);
            #endif
            
            //p
            mpz_add(P_i, tmp1, tmp2);
            gmp_printf("p = 0x%Zx\n", P_i);
            
            //q
            mpz_sub(P_i, tmp2, tmp1);
            gmp_printf("q = 0x%Zx\n", P_i);
            break;
        }
    }   
    
    mpz_clear(P_i);
    mpz_clear(Q_i);
    mpz_clear(P_i_2);
    mpz_clear(Q_i_2);
    mpz_clear(tmp1);
    mpz_clear(tmp2);
}

static inline size_t euclid_algo(mpz_t *res, const mpz_t *a, const mpz_t *b)
{
    register size_t i = 0;
    mpz_t n, d, tmp;
    mpz_init_set(n, *a);
    mpz_init_set(d, *b);
    mpz_init(tmp);
    
    do
    {
        mpz_tdiv_qr(res[i], tmp, n, d);
        #ifdef DEBUG
        gmp_printf("%Zd / %Zd = %Zd\n, rest = %Zd\n", n, d, res[i], tmp);
        #endif
        mpz_set(n, d);
        mpz_set(d, tmp);
        ++i;
    } while (mpz_cmp_ui(tmp, 0ul) != 0);
    
    mpz_clear(n);
    mpz_clear(d);
    mpz_clear(tmp);
    
    return i;
}

int main(int argc, char* argv[])
{
    #ifdef DEBUG
    register_handler();
    #endif
    
    mpz_t e, n;
    register size_t i;

    if (argc < ARGS_COUNT)
    {
        fprintf(stderr, "Not enough arguments!");
        exit(EXIT_FAILURE);
    }    
    
    mpz_init_set_str(e, argv[2], 16);
    mpz_init_set_str(n, argv[4], 16);
    
    mpz_t cont_fraq[MAX_SIZE];
    
    for (i = 0; i < MAX_SIZE; ++i)
    {
        mpz_init(cont_fraq[i]);
    }
    
    register size_t cont_fraq_n = euclid_algo(cont_fraq, &e, &n);
    calc_d(cont_fraq, cont_fraq_n, &e, &n);
    
    for (i = 0; i < MAX_SIZE; ++i)
    {
        mpz_clear(cont_fraq[i]);
    }
    
    mpz_clear(e);
    mpz_clear(n);
    
    exit(EXIT_SUCCESS);
}